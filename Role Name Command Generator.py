#this script is designed to succinctly and repetitively pump out the commands needed to obtain the policy titles & 
#policy limitations of a role in AWS when you only know the name of the role

role_name = raw_input("Name of the role:")

print "iam list-role-policies --role-name " + role_name

policy_name = raw_input("Name of the policy:")

print "iam get-role-policy --role-name " + role_name + " --policy-name " + policy_name